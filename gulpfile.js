'use strict';

/* параметры для gulp-autoprefixer */
var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

/* пути к исходным файлам (src), к готовым файлам (build), а также к тем, за изменениями которых нужно наблюдать (watch) */
var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: {
        pug: 'source/templates/index.pug',
        js: 'source/js/main.js',
        style: './source/styles/main.scss',
        img: 'source/img/**/*.*',
        fonts: 'source/fonts/**/*.*'
    },
    watch: {
        pug: 'source/templates/**/*.pug',
        js: 'source/js/**/*',
        css: 'source/styles/**/*',
        img: 'source/img/**/*',
        fonts: 'source/fonts/**/*.*'
    },
    clean: './build/*'
};


/* подключаем gulp и плагины */
const   gulp           = require('gulp'),
        browserSync    = require('browser-sync').create(),
        pug 	       = require('gulp-pug'),
        sass 	       = require('gulp-sass'),
        plumber        = require('gulp-plumber'), // модуль для отслеживания ошибок
        prefixer       = require('gulp-autoprefixer'),
        sourcemaps     = require('gulp-sourcemaps'),
        spritesmith    = require('gulp.spritesmith'),
        cleanCSS       = require('gulp-clean-css'), // плагин для минимизации CSS
        uglify         = require('gulp-uglify'), // модуль для минимизации JavaScript
        cache          = require('gulp-cache'), // модуль для кэширования
        //imagemin       = require('gulp-imagemin'), // плагин для сжатия PNG, JPEG, GIF и SVG изображений
        //jpegrecompress = require('imagemin-jpeg-recompress'), // плагин для сжатия jpeg
        //pngquant       = require('imagemin-pngquant'), // плагин для сжатия png
        rimraf         = require('gulp-rimraf'), // плагин для удаления файлов и каталогов
        rename         = require('gulp-rename');

      sass.compiler    = require('node-sass');

/* задачи */

// ========= Static server ===============
gulp.task('server', function() {
    browserSync.init({
        server: {
            port: 1488,
            baseDir: 'build'
        }
    });

    gulp.watch(path.watch.pug).on('change', browserSync.reload);
});

// ============== PUG =============== //
gulp.task('html:build', function buildHTML() {
  return gulp.src(path.src.pug)
      .pipe(plumber())
      .pipe(pug({
          pretty: true
      }))
      .pipe(gulp.dest(path.build.html))
});

// ============== scss =============== //
gulp.task('css:build', function () {
  return gulp.src(path.src.style)
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer( { // добавим префиксы
                browsers: autoprefixerList
            }
        ))
        .pipe(gulp.dest(path.build.css))
        .pipe(rename({ suffix: '.min' }))
        .pipe(cleanCSS()) // минимизируем CSS
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css));
});

// сбор js
gulp.task('js:build', function () {
    return gulp.src(path.src.js) // получим файл main.js
        .pipe(plumber()) // для отслеживания ошибок
        .pipe(gulp.dest(path.build.js))
        .pipe(rename({ suffix: '.min' }))
        .pipe(sourcemaps.init()) //инициализируем sourcemap
        .pipe(uglify()) // минимизируем js
        .pipe(sourcemaps.write('./')) //  записываем sourcemap
        .pipe(gulp.dest(path.build.js)) // положим готовый файл
});


// ================ SPRITES =====================
gulp.task('sprite', function () {
  var spriteData = gulp.src('source/images/icons/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    cssName: 'sprite.css',
    imgPath: 'source/img/sprite.png'
  }));
  return spriteData.pipe(gulp.dest('build/images/'));
});

gulp.task('copy:fonts', function(){
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts));
});

gulp.task('copy:images', function(){
	return gulp.src(path.src.img)
		.pipe(gulp.dest(path.build.img));
});

gulp.task('copy', gulp.parallel('copy:fonts', 'copy:images'))

// удаление каталога build
gulp.task('clean:build', function () {
    return gulp.src(path.clean, { read: false })
        .pipe(rimraf());
});

// очистка кэша
gulp.task('cache:clear', function () {
    cache.clearAll();
});

// сборка
gulp.task('build',
    gulp.series('clean:build',
        gulp.parallel(
            'html:build',
            'css:build',
            'sprite',
            'copy'
        )
    )
);

// запуск задач при изменении файлов
gulp.task('watch', function(){
    gulp.watch(path.watch.pug, gulp.series('html:build'));
    gulp.watch(path.watch.css, gulp.series('css:build'));
});

// задача по умолчанию
gulp.task('default', gulp.series(
    'build',
	gulp.parallel('watch', 'server')
	)
);